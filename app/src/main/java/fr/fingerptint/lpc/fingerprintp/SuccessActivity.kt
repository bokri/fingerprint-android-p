package fr.fingerptint.lpc.fingerprintp

import android.app.Activity
import android.os.Bundle

class SuccessActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
    }
}
