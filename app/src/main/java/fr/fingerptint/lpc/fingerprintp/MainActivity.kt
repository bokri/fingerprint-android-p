package fr.fingerptint.lpc.fingerprintp

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.hardware.fingerprint.FingerprintDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CancellationSignal
import android.util.Log
import android.widget.Toast

class MainActivity : AppCompatActivity(), DialogInterface.OnClickListener {

    lateinit var cancellationSignal: CancellationSignal


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dialog = FingerprintDialog.Builder()
                .setTitle("Title")
                .setSubtitle("Subtitle")
                .setDescription("Description")
                .setNegativeButton("Cancel", mainExecutor, this)
                .build(this)

        val callback = AuthCallback(applicationContext)

        cancellationSignal = CancellationSignal()


        dialog.authenticate(cancellationSignal, mainExecutor, callback)
    }

    override fun onClick(dialogInternace: DialogInterface?, int: Int) {
        Toast.makeText(this, "Canceled", Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        cancellationSignal.cancel()
        super.onStop()

    }
}

class AuthCallback(private var context: Context): FingerprintDialog.AuthenticationCallback() {


    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
        Log.e("MAIN_ACTIVITY", "Authentication Error "+ errorCode.toString() + errString)


        /*

            public static final int FINGERPRINT_ERROR_CANCELED = 5;
            public static final int FINGERPRINT_ERROR_HW_NOT_PRESENT = 12;
            public static final int FINGERPRINT_ERROR_HW_UNAVAILABLE = 1;
            public static final int FINGERPRINT_ERROR_LOCKOUT = 7;
            public static final int FINGERPRINT_ERROR_LOCKOUT_PERMANENT = 9;
            public static final int FINGERPRINT_ERROR_NO_FINGERPRINTS = 11;
            public static final int FINGERPRINT_ERROR_NO_SPACE = 4;
            public static final int FINGERPRINT_ERROR_TIMEOUT = 3;
            public static final int FINGERPRINT_ERROR_UNABLE_TO_PROCESS = 2;
            public static final int FINGERPRINT_ERROR_USER_CANCELED = 10;
            public static final int FINGERPRINT_ERROR_VENDOR = 8;

         */
    }

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence) {

        /*
            public static final int FINGERPRINT_ACQUIRED_GOOD = 0;
            public static final int FINGERPRINT_ACQUIRED_IMAGER_DIRTY = 3;
            public static final int FINGERPRINT_ACQUIRED_INSUFFICIENT = 2;
            public static final int FINGERPRINT_ACQUIRED_PARTIAL = 1;
            public static final int FINGERPRINT_ACQUIRED_TOO_FAST = 5;
            public static final int FINGERPRINT_ACQUIRED_TOO_SLOW = 4;

         */

        Log.e("MAIN_ACTIVITY", "Authentication Help " + helpCode.toString() + helpString)
    }

    override fun onAuthenticationSucceeded(result: FingerprintDialog.AuthenticationResult) {
        val intent = Intent(context, SuccessActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)

        Log.v("MAIN_ACTIVITY", "Authentication Succeed")
    }

    override fun onAuthenticationFailed() {
        Log.e("MAIN_ACTIVITY", "Authentication Failed")
    }
}
